<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@ page import="fx62.org.complements.*, java.util.ArrayList, java.util.List" contentType="text/html; charset=UTF-8"%>

<html>
    <head>
        <title>Doubly Linked List</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container align-content-between">
            <div class="row">
                <div class="col-4">
                    <div class="alert alert-success" role="alert">
                        <h5><b>Add to doubly linked list: </b></h5>
                    </div>
                    <form name="OutputUser" action="index.jsp">
                        <p>
                            <label>
                                <input type="checkbox" name="student" value="370,Andrea Martínez" >
                                370, "Andrea Martínez" </label>
                            <br>
                            <label>
                                <input type="checkbox" name="student" value="762,Mónica Monroy" >
                                762, "Mónica Monroy" </label>
                            <br>
                            <label>
                                <input type="checkbox" name="student" value="661,Arturo Diaz" >
                                661, "Arturo Diaz" </label>
                            <br>
                            <label>
                                <input type="checkbox" name="student" value="642,Carlos López" >
                                642, "Carlos López" </label>
                            <br>
                            <label>
                                <input type="checkbox" name="student" value="590,Marlon Sánchez" >
                                590, "Marlon Sánchez" </label>
                            <br>
                        </p>
                        <p>
                            <button type="input" class="btn btn-lg btn-primary" name="AddUp">Add up</button>
                            <button type="input" class="btn btn-lg btn-primary" name="AddDown">Add down</button>
                            <br>
                        </p>
                    </form>
                    <form name="delete one user" action="index.jsp">
                        <button type="input" class="btn btn-danger" name="DeleteUp">Delete up</button>
                        <button type="input" class="btn btn-danger" name="DeleteDown">Delete down</button>

                    </form>
                </div>
                <div class="col-8">
                    <div class="alert alert-primary" role="alert">
                        <h5><b>Actual Doubly Linked List Students</b></h5>
                    </div>
                    <%
                        List<Object> show;
                        DoublyLinkedList list;
                        if (request.getParameter("AddUp") != null || request.getParameter("AddDown") != null) {
                            list = (DoublyLinkedList) session.getAttribute("students");
                            if (list == null) {
                                list = new DoublyLinkedList();
                                session.setAttribute("students", list);
                            }
                            String[] students = request.getParameterValues("student");
                            if (students != null) {
                                int id;
                                String name;
                                String[] split;
                                for (String student : students) {
                                    split = student.split(",");
                                    id = Integer.parseInt(split[0]);
                                    name = split[1];
                                    //out.print(id + "-" + name + "<br>");
                                    Student temp = new Student(id, name);
                                    if(request.getParameter("AddUp") != null){
                                        list.addLeft(temp);
                                    } else if(request.getParameter("AddDown") != null) {
                                        list.addRight(temp);
                                    }
                                    //list.add(student);
                                    //list.addLeft(new Student(id, name));
                                }
                                show = list.showFromLeft();
                                if (show != null) {
                                    for (int i = 0; i < show.size(); i++) {
                                        Student student  = (Student)show.get(i);
                                        out.print(student.getId() + " - " + student.getNombre() + "<br>");
                                    }
                                }
                            }
                        } else if (request.getParameter("DeleteUp") != null || request.getParameter("DeleteDown") != null) {
                            list = (DoublyLinkedList) session.getAttribute("students");
                            if (list != null) {
                                if(request.getParameter("DeleteUp") != null){
                                    list.deleteLeft();
                                } else if(request.getParameter("DeleteDown") != null) {
                                    list.deleteRight();
                                }
                                session.setAttribute("students", list);
                                show = list.showFromLeft();
                                if (show != null) {
                                    for (int i = 0; i < show.size(); i++) {
                                        Student student  = (Student)show.get(i);
                                        out.print(student.getId() + " - " + student.getNombre() + "<br>");
                                    }
                                }
                            } 
                        }
                    %>
                </div>
            </div>
        </div>
    </body>
</html>
