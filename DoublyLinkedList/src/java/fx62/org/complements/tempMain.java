/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx62.org.complements;

import java.util.List;

/**
 *
 * @author fx62
 */
public class tempMain {

    public static void main(String[] args) {
        DoublyLinkedList list = new DoublyLinkedList();
        list.addRight(new Student(1, "Andrea Martínez"));
        list.addRight(new Student(2, "Mónica Monroy"));
        list.addRight(new Student(3, "Arturo Diaz"));
        list.addLeft(new Student(7, "Marco Hernandez"));
        list.addRight(new Student(4, "Carlos López"));
        list.addRight(new Student(5, "Marlon Sánchez"));
        list.addLeft(new Student(6, "Lucero Mayorga"));
        List<Object> students = list.showFromLeft();
        for (int i = 0; i < students.size(); i++) {
            Student student = (Student) students.get(i);
            System.out.println(student.getId() + " - " + student.getNombre());
        }
        list.deleteLeft();
        list.deleteRight();
        list.deleteLeft();
        list.deleteRight();
        list.deleteLeft();
        list.deleteRight();
        list.deleteLeft();
        System.out.print(list.isEmpty());
        students = list.showFromRight();
        if (students != null) {
            for (int i = 0; i < students.size(); i++) {
                Student student = (Student) students.get(i);
                System.out.println(student.getId() + " - " + student.getNombre());
            }
        }
//        list.showFromRight();
//        list.showFromLeft();
//        list.deleteRight();
//        list.showFromLeft();
//        list.deleteLeft();
//        list.showFromLeft();
//        list.showFromRight();
        //list.showFromRight();

    }
}
